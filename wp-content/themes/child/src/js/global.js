jQuery(document).ready(function($) {

	/* Sticky footer */
/*
	var bumpIt = function() {
			$('html').css('min-height', $(window).height() - $('#wpadminbar').height());
			$('body').css('margin-bottom', $('footer.sticky').height());
			$('body.admin-bar').css('margin-bottom', $('footer.sticky').height() + 0);
		},
		didResize = false;
	bumpIt();
	$(window).resize(function() {
		didResize = true;
	});
	setInterval(function() {
		if (didResize) {
			didResize = false;
			bumpIt();
		}
	}, 250);
*/
	
	/* Fancybox init */
	$(".fancybox").fancybox({
		prevEffect		: 'none',
		nextEffect		: 'none',
		closeBtn		: false,
		helpers		: {
			title	: { type : 'inside' },
			buttons	: {}
		}
	});

	/* Minimera navbar on scroll */
	$(document).on("scroll",function(){
	    if($(document).scrollTop()>100){
	        $(".navbar-fixed-top").removeClass("navBig").addClass("navSmall");
	        $(".nav-margin").removeClass("navBig").addClass("navSmall");
	    } else{
	        $(".navbar-fixed-top").removeClass("navSmall").addClass("navBig");
	        $(".nav-margin").removeClass("navSmall").addClass("navBig");
	    }
	});
	
	//Smooth anchor scrolling

	$('a[href*=#]:not([href=#])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
	        || location.hostname == this.hostname) {
	
	        var target = $(this.hash);
	        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	           if (target.length) {
	             $('html,body').stop().animate({
	                 scrollTop: target.offset().top
	            }, 1000);
	            return false;
	        }
	    }
	});

});



