<?php

//Default values
$cols = 2;
$imgstyle = "landscape";
$lastcol = "";
$colwidth = "6";

$img_1 = get_stylesheet_directory_uri() . "/src/images/default.png";
$rubrik_1 = "Rubrik 1";
$text_1 = "Text...";
$link_1 = "#";
$img_class = "";
$align_class = "";
$text_align_class = "";

$img_2 = get_stylesheet_directory_uri() . "/src/images/default.png";
$rubrik_2 = "Rubrik 2";
$text_2 = "Text...";
$link_2 = "#";

$img_3 = get_stylesheet_directory_uri() . "/src/images/default.png";
$rubrik_3 = "Rubrik 3";
$text_3 = "Text...";
$link_3 = "#";


// Collect ACF values
if(get_sub_field('bild_1')):
	$bild_1 = get_sub_field('bild_1');
endif;
if(get_sub_field('rubrik_1')):
	$rubrik_1 = get_sub_field('rubrik_1');
endif;
if(get_sub_field('text_1')):
	$text_1 = get_sub_field('text_1');
endif;

if(get_sub_field('link_1')):
	$link_1 = get_sub_field('link_1');
endif;

if(get_sub_field('bild_2')):
	$bild_2 = get_sub_field('bild_2');
endif;
if(get_sub_field('rubrik_2')):
	$rubrik_2 = get_sub_field('rubrik_2');
endif;
if(get_sub_field('text_2')):
	$text_2 = get_sub_field('text_2');
endif;

if(get_sub_field('link_2')):
	$link_2 = get_sub_field('link_2');
endif;

if(get_sub_field('bild_3')):
	$bild_3 = get_sub_field('bild_3');
endif;
if(get_sub_field('rubrik_3')):
	$rubrik_3 = get_sub_field('rubrik_3');
endif;
if(get_sub_field('text_3')):
	$text_3 = get_sub_field('text_3');
endif;

if(get_sub_field('link_3')):
	$link_3 = get_sub_field('link_3');
endif;

if(get_sub_field('antal_kolumner')):
	$cols = get_sub_field('antal_kolumner');
endif;
if(get_sub_field('bildformat')):
	$imgstyle = get_sub_field('bildformat');
endif;

//Get img-url from array
if( !empty($bild_1)  && $imgstyle == "landscape"):
	$img_1 = $bild_1['sizes'][ 'Landscape' ];
elseif( !empty($bild_1)  && $imgstyle == "square"):
	$img_1 = $bild_1['sizes'][ 'Square' ];
elseif( !empty($bild_1)  && $imgstyle == "portrait"):
	$img_1 = $bild_1['sizes'][ 'Portrait' ];
elseif( !empty($bild_1)  && $imgstyle == "circle"):
	$img_1 = $bild_1['sizes'][ 'Square' ];
	$img_class = "img-circle";
	$align_class = "center-block";
	$text_align_class = "text-center";
endif;

if( !empty($bild_2)  && $imgstyle == "landscape"):
	$img_2 = $bild_2['sizes'][ 'Landscape' ];
elseif( !empty($bild_2)  && $imgstyle == "square"):
	$img_2 = $bild_2['sizes'][ 'Square' ];
elseif( !empty($bild_2)  && $imgstyle == "portrait"):
	$img_2 = $bild_2['sizes'][ 'Portrait' ];
elseif( !empty($bild_2)  && $imgstyle == "circle"):
	$img_2 = $bild_2['sizes'][ 'Square' ];
endif;

if( !empty($bild_3)  && $imgstyle == "landscape"):
	$img_3 = $bild_3['sizes'][ 'Landscape' ];
elseif( !empty($bild_3)  && $imgstyle == "square"):
	$img_3 = $bild_3['sizes'][ 'Square' ];
elseif( !empty($bild_3)  && $imgstyle == "portrait"):
	$img_3 = $bild_3['sizes'][ 'Portrait' ];
elseif( !empty($bild_3)  && $imgstyle == "circle"):
	$img_3 = $bild_3['sizes'][ 'Square' ];
endif;

if($cols == 3):
	$lastcol = '<div class="col-sm-4"><a href="' . $link_3 . '"><img style="margin-bottom:30px;" class="' . $img_class . ' ' .  $align_class . '" src="' . $img_3 . '"></a>' . $text_3 . '</div>' ;
	$colwidth = "4";
endif;







$str = <<<EOF

      	<section class="row puffar">
				<div class="col-sm-$colwidth">
					<a href="$link_1"><img style="margin-bottom:30px;" class="$img_class $align_class" src="$img_1"></a>
					
					$text_1
				</div>		      		
				<div class="col-sm-$colwidth">
					<a href="$link_2"><img style="margin-bottom:30px;" class="$img_class $align_class" src="$img_2"></a>
					
					$text_2
				</div>
				$lastcol
						      		
		</section><!-- end .row -->
		
EOF;
