<?php
	
// Default values
$settings = "full";
$text = "Text på väg";
$output = "<section class='row'><div class='col-sm-12'>" . $text . "</div></section>";


// Collect ACF values
if(get_sub_field('text')):
	$text = get_sub_field('text');
endif;

if(get_sub_field('styckeinstallning')):
	$settings = get_sub_field('styckeinstallning');
endif;


// Set variables
if($settings == "narrow"):
	$output = "<section class='row'><div class='col-sm-8 col-sm-offset-2'>" . $text . "</div></section>";	
endif;

if($settings == "twocols"):
	$output = "<div class='twocols'>" . $text . "</div>";	
endif;

if($settings == "full"):
	$output = "<div class='fullwidth'>" . $text . "</div>";	
endif;


$str = <<<EOF

<section>
		$output	
</section>
		
EOF;

