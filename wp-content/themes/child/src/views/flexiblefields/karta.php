<?php
$text = "Text på väg";

// Collect ACF values
if(get_sub_field('adress')):
	$map = get_sub_field('adress');
    $lat = $map["lat"];
    $lng = $map["lng"];
endif;

if(get_sub_field('text')):
	$text = get_sub_field('text');
endif;

$setting = get_sub_field('extratext');


switch ($setting) {
    case "text_left":
        $leftcol = '<div class="col-sm-4">' . $text . '</div>';
        $rightcol = "";
		$mapcol = "8";       
        break;
    case "text_right":
        $leftcol = "";
        $rightcol = '<div class="col-sm-4">' . $text . '</div>';
		$mapcol = "8";       
        break;
    default:
        $leftcol = "";
        $rightcol = "";
		$mapcol = "12";       
}





$str = <<<EOF


<style>
      #map-canvas {
        width: 100%;
        height:  300px ;
      }
</style>

<script src="https://maps.googleapis.com/maps/api/js"></script>
<script>

  function initialize() {

  	var startLatlng = new google.maps.LatLng($lat , $lng);

    var mapCanvas = document.getElementById('map-canvas');
    var mapOptions = {
      center: startLatlng,
      zoom: 14,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var map = new google.maps.Map(mapCanvas, mapOptions);

    var marker = new google.maps.Marker({
 		 position: startLatlng,
  		 map: map,
  		 title: 'Här finns vi!'
});


  }
  google.maps.event.addDomListener(window, 'load', initialize);

</script>




<section class="row">
	$leftcol
	<div class="col-sm-$mapcol">
		<div id="map-canvas"></div>
	</div>
	$rightcol
</section>































EOF;
