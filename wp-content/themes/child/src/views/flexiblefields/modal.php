<?php
$imageArray = get_sub_field("bild");
$imgUrl = $imageArray["sizes"]["Pagewidth"];


$rubrik = get_sub_field("rubrik-modal") ;
$copy = get_sub_field("innehall_i_modal") ;

$modalid = substr(str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 5);





$str = <<<EOF


<article class="col-sm-12" style="margin-bottom:30px;">
	<a href="#" class="modaltrigger" data-toggle="modal" data-target="#$modalid"><img src="$imgUrl"></a>
</article>
<div class="modal fade" id="$modalid" tabindex="-1" role="dialog" aria-labelledby="$modalid" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Stäng</span></button>
        <h4 class="modal-title" id="myModalLabel">$rubrik </h4>
      </div>
      <div class="modal-body">
       $copy
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Stäng</button>
      </div>
    </div>
  </div>
</div>



EOF;
//echo $str;



?>