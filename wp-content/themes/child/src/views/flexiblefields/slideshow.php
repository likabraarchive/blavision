<?php

// Default values
$output ="";
$dots = "true";
$transition = "false";
$autoplay = "true";
$arrows = "false";
$loop = "true";
$numberofslides = "3";
$autoplayspeed ="4000";
$id ="slideshow";

// Collect ACF values
$images = get_sub_field('bilder');

if(get_sub_field('dots') == 0):
	$dots = "false";
else:
	$dots = "true";
endif;
if(get_sub_field('transition') == 0):
	$transition = "false";
else:
	$transition = "true";
endif;
if(get_sub_field('autoplay')== 0):
	$autoplay = "false";
else:
	$autoplay = "true";
endif;
if(get_sub_field('autoplayspeed')):
	$autoplayspeed = get_sub_field('autoplayspeed');
endif;
if(get_sub_field('arrows')== 0):
	$arrows = "false";
else:
	$arrows = "true";
endif;
if(get_sub_field('loop')== 0):
	$loop = "false";
else:
	$loop = "true";
endif;
if(get_sub_field('numberofslides')):
	$numberofslides = get_sub_field('numberofslides');
endif;
if(get_sub_field('id')):
	$id = get_sub_field('id');
endif;


$imgformat = get_sub_field('bildformat');


if( $images ):
    foreach( $images as $image ):
	    $imgdesc = "#";
		$desc = substr($image['description'], 0, 7);
		if($desc == 'http://'):
			$imgdesc = $image['description'];
		endif;
		
		$caption = "";
    	if($image['caption']):
    		$caption = '<div class="sliderdesc-many">' . $image['caption'] . '</div>';
    	endif;


    	$output .= '<div style="padding: 10px;"><a href="' . $imgdesc . '"><img src="' . $image['sizes'][$imgformat] . '" /></a>' . $caption . '</div>';
    endforeach;
endif;

if($numberofslides != 1):
	$transition = "false";
endif;	


$str = <<<EOF


<section style="padding-bottom: 1px; overflow: hidden;">
	<div id="$id" style="margin-left:-10px;margin-right:-10px">
		$output
	</div>
</section>

<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('#$id').slick({
		  dots: $dots,
		  speed: 2000,
		  fade: $transition,
		  autoplay: $autoplay,
		  autoplaySpeed: $autoplayspeed,
		  arrows: $arrows,
		  cssEase: 'ease',
		  infinite: $loop,
		  slidesToShow: $numberofslides,
		  slidesToScroll: $numberofslides,
		});
});
</script>


EOF;
