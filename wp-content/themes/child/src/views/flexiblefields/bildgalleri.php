<?php

// Collect ACF values
$images = get_sub_field('bilder');
$output ="";


if( $images ):
    foreach( $images as $image ):
    	$output .= '<a rel="group" title="' . $image['title'] . '" class="fancybox col-sm-2" href="' . $image['url'] . '"><img class="galleri-item" src="' . $image['sizes']['thumbnail'] . '" alt="' . $image['alt'] . '" /></a>';
    endforeach;
endif;	


$str = <<<EOF

<section class="row">
	$output
</section>

EOF;
