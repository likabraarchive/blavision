<?php

$bgcolor = 

$color = "transparent";
$image ="none";
$repeat = "repeat";
$position = "initial";
$size = "auto";
$contentbg = "transparent";
$contentcolor = "inherit";
$contentmargin = "0";
$anchor = "";

$set_color = get_sub_field("farg");
$set_image =get_sub_field("bild");
$set_settings = get_sub_field("settings");
$set_contentbg = get_sub_field("contentfarg");
$set_contentcolor = get_sub_field("textfarg");
$set_contentmargin = get_sub_field("marginal");
$anchor = get_sub_field("ankarlank");


if($set_color ) {
	$color = $set_color;
};

if($set_image ) {
	$image = "url(" . $set_image . ")";
};

if($set_settings == "no-repeat" ) {
	$repeat = "no-repeat";
	$position = "center center";
	$size ="cover";
};

if($set_contentbg ) {
	$contentbg = $set_contentbg;
};

if($set_contentcolor ) {
	$contentcolor = $set_contentcolor;
};

if($set_contentmargin ) {
	$contentmargin = $set_contentmargin . "px";
};




$str = <<<EOF

	</div><!-- end .container -->
</section><!-- end #page -->

<section class="pagesection" style="position:relative;background-color: $color; background-image:$image;background-repeat:$repeat;background-position:$position;background-size:$size;">
	  <a class="anchor" name="$anchor"></a>
      <div class="container" style="background-color:$contentbg;color:$contentcolor;padding-top:$contentmargin;padding-bottom:$contentmargin;">

EOF;
