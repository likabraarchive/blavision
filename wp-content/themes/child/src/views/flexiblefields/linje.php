<?php
	
// Default values
$color = "#eee";


// Collect ACF values
if(get_sub_field('farg')):
	$color = get_sub_field('farg');
endif;



$str = <<<EOF


<hr style="border-top: 1px solid $color;">

EOF;
