<!-- Kontakt template-->

<?php


$textfield = get_sub_field("textfalt");

$text = get_sub_field("extratext");



switch ($textfield) {
    case "before":
        $before = '<section style="margin-bottom: 60px;" class="col-sm-3" >' . $text . '</section>';
        $after = '';
        break;
    case "after":
        $before = '';
        $after = '<section style="margin-bottom: 60px;" class="col-sm-3" >' . $text . '</section>';
        break;
    default:
        $before = '';
        $after = '';
}


// check if the repeater field has rows of data
if( have_rows('person') ):

 	// loop through the rows of data
    while ( have_rows('person') ) : the_row();

		$imageArray = get_sub_field("bild");
		$imgUrl = $imageArray["sizes"]["Portrait"];
		$namn = get_sub_field("namn") ;
		$titel = get_sub_field("titel") ;
		$telefon = get_sub_field("telefon") ;
		$epost = get_sub_field("epost") ;

		$kontaktperson .= '<section style="margin-bottom: 60px;" class="col-sm-3"><img src="' . $imgUrl . '" /><p style="margin-top:20px;"><span class="bla-namn">' . $namn . '</span><br><span class="bla-titel">' . $titel . '</span></p><p></p><span class="bla-mail"><a href="mailto:' . $epost . '">' . $epost . '</a></span><br><span class="bla-tel">' . $telefon . '</span></section>'	;	
    endwhile;

else :

    // no rows found

endif;






$str = <<<EOF


    


	<section class="row">
			$before
			$kontaktperson
			$after
	</section>




<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('.equalheight').responsiveEqualHeightGrid();
});
</script>



EOF;
