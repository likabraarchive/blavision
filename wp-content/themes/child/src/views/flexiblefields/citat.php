<?php
	
// Default values
$intro = "Introtext";
$citat ="Citattexten ser ut så här";
$byline = "-Detta är en byline";
$img = get_stylesheet_directory_uri() . "/src/images/default.png";
$position ="right";
$bildstil = "circle";

$firstcol = 5;
$secondcol = 3;
$content_left = "";
$content_right = "";
$citatblock = "";


// Collect ACF values
if(get_sub_field('bild') && $bildstil == "circle"):
	$img = get_sub_field('bild');
	$img = '<img class="img-circle citalbild" src="' . $img['sizes'][ 'Square' ] . '">';
else:
	$img = get_sub_field('bild');
	$img = '<img class="citatbild" src="' . $img['sizes'][ 'Square' ] . '">';
endif;

if(get_sub_field('intro')):
	$intro = '<div class="intro">' . get_sub_field('intro') . '</div>';
endif;
if(get_sub_field('citat')):
	$citat =  '<div class="citat">' . get_sub_field('citat') . '</div>';
endif;
if(get_sub_field('byline')):
	$byline =  '<div class="byline">' . get_sub_field('byline') . '</div>';
endif;

$citatblock = '<div class="vertical-align">' . $intro . $citat . $byline . '</div>';


if(get_sub_field('position') == "right"):
	$firstcol = 5;
	$secondcol = 3;
	$content_left = $citatblock;
	$content_right = $img;
else:
	$firstcol = 3;
	$secondcol = 5;
	$content_left = $img;
	$content_right = $citatblock;
endif;



// Output






$str = <<<EOF

<section class="row">
	<div class="col-sm-$firstcol col-sm-offset-2 equalheight">
			$content_left
	</div>
	<div class="col-sm-$secondcol equalheight">
			$content_right
	</div>
</section>

<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('.equalheight').responsiveEqualHeightGrid();
});
</script>


EOF;
