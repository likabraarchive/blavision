<?php

// Default values
$output ="";
$dots = "true";
$transition = "false";
$autoplay = "true";
$arrows = "false";
$loop = "true";
$numberofslides = "1";
$autoplayspeed ="4000";
$id ="slideshow";
$title = "";

// Collect ACF values
$images = get_sub_field('bilder');

if(get_sub_field('dots') == 0):
	$dots = "false";
else:
	$dots = "true";
endif;
if(get_sub_field('transition') == 0):
	$transition = "false";
else:
	$transition = "true";
endif;
if(get_sub_field('autoplay')== 0):
	$autoplay = "false";
else:
	$autoplay = "true";
endif;
if(get_sub_field('autoplayspeed')):
	$autoplayspeed = get_sub_field('autoplayspeed');
endif;
if(get_sub_field('arrows')== 0):
	$arrows = "false";
else:
	$arrows = "true";
endif;
if(get_sub_field('loop')== 0):
	$loop = "false";
else:
	$loop = "true";
endif;
if(get_sub_field('numberofslides')):
	$numberofslides = get_sub_field('numberofslides');
endif;
if(get_sub_field('id')):
	$id = get_sub_field('id');
endif;
if(get_sub_field('showtitle') == "ja"):
	$title = '<div class="slidertitle"><div class="container"><h1><span>' . get_the_title() . '</span></h1></div></div>';
endif;

if( $images ):
    foreach( $images as $image ):
    	$caption = "";
    	if($image['caption']):
    		$caption = '<div class="sliderdesc">' . $image['caption'] . '</div>';
    	endif;
    	$output .= '<div>' . $caption . '<img src="' . $image['sizes']['Wide'] . '" alt="' . $image['alt'] . '" /></div>';
    endforeach;
endif;

if($numberofslides != 1):
	$transition = "false";
endif;	


$str = <<<EOF

	</div>
</section><!-- end #page -->


<section style="margin-bottom:-30px;position:relative;">
	$title
	<div id="$id">
		$output
	</div>
</section>

<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('#$id').slick({
		  dots: $dots,
		  speed: 2000,
		  fade: $transition,
		  autoplay: $autoplay,
		  autoplaySpeed: $autoplayspeed,
		  arrows: $arrows,
		  cssEase: 'ease',
		  infinite: $loop,
		  slidesToShow: $numberofslides,
		  slidesToScroll: $numberofslides,
		});
});
</script>

<section class="afterslide pagesection">
	<div class="container">



EOF;
