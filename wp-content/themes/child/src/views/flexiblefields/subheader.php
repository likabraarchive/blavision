<?php

$subtitle = get_sub_field('text');
$farg = get_sub_field('farg');

$center =  ' style="color:' . $farg . ';"';

if( get_sub_field('centrera') )
{
    $center = ' style="color:' . $farg . '; text-align:center;"';
}


$str = <<<EOF

<section>
	<h3$center>$subtitle</h3>
</section>

EOF;
