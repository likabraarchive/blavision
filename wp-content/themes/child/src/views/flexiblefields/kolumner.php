<?php

// Default values
$settings = "six-six";
$text_1 = "Text ett på väg";
$text_2 = "Text två på väg";
$text_3 = "Text tre på väg";
$output = "<section class='row'><div class='col-sm-6'>" . $text_1 . "</div><div class='col-sm-6'>" . $text_2 . "</div></section>";


// Collect ACF values
if(get_sub_field('antal_kolumner')):
	$settings = get_sub_field('antal_kolumner');
endif;

if(get_sub_field('text_1')):
	$text_1 = get_sub_field('text_1');
endif;

if(get_sub_field('text_2')):
	$text_2 = get_sub_field('text_2');
endif;

if(get_sub_field('text_3')):
	$text_3 = get_sub_field('text_3');
endif;


// Set variables
if($settings == "six-six"):
	$output = "<section class='row'><div class='col-sm-6'>" . $text_1 . "</div><div class='col-sm-6'>" . $text_2 . "</div></section>";
endif;

if($settings == "four-eight"):
	$output = "<section class='row'><div class='col-sm-4'>" . $text_1 . "</div><div class='col-sm-8'>" . $text_2 . "</div></section>";
endif;

if($settings == "eight-four"):
	$output = "<section class='row'><div class='col-sm-8'>" . $text_1 . "</div><div class='col-sm-4'>" . $text_2 . "</div></section>";
endif;

if($settings == "four-four-four"):
	$output = "<section class='row'><div class='col-sm-4'>" . $text_1 . "</div><div class='col-sm-4'>" . $text_2 . "</div><div class='col-sm-4'>" . $text_3 . "</div></section>";
endif;



$str = <<<EOF

	$output

EOF;
