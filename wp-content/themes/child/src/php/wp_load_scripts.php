<?php

// Register Styles
function child_theme_styles() {

	wp_register_style( 'default', get_stylesheet_directory_uri() . '/src/css/child.css', array('likabra'), false );
	wp_enqueue_style( 'default' );
}
// Hook into the 'wp_enqueue_scripts' action
add_action( 'wp_enqueue_scripts', 'child_theme_styles' );



// Register Scripts
function child_theme_scripts() {

	wp_register_script( 'child', get_stylesheet_directory_uri() . '/src/js/child.js', array( 'bootstrap' ), false, true ); // load in footer
	wp_enqueue_script( 'child' );
}
// Hook into the 'wp_enqueue_scripts' action
add_action( 'wp_enqueue_scripts', 'child_theme_scripts' );
