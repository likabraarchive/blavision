<?php 
/**
 * Template Name: Fri layout
 */

get_header(); ?>

<!-- Start the loop -->
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>


<section id="page" class="pagesection">
      <div class="container">
	      
<?php
	$flexibel = new LBFlexibleField("layout");
	echo $flexibel->returnHTML();
?>

	</div>
</section><!-- end #page -->





<!-- End the loop -->
<?php		
		endwhile;
			
		else :
		// If no content...
		echo '<section><div class="container">Ingenting hittades...</div></section>';
	endif;
?>

<script type="text/javascript">
	jQuery(document).ready(function($) {
		
		if ( $.trim( $('#page').text() ) == "")
		    $('#page').remove();
		    
		if ( $.trim( $('.afterslide').text() ) == "")
		    $('.afterslide').remove();
	});
	
	
</script>

      
<?php get_footer(); ?>      



