<?php


if ( ! function_exists('custom_code') ) {

// Register Custom Code
require_once("src/php/classes.php");
require_once('src/php/wp_load_scripts.php');
require_once('src/php/custom_post_types.php');
require_once('src/php/wp_bootstrap_navwalker.php');

}




if ( ! function_exists('custom_theme_features') ) {

// Register Theme Features
function custom_theme_features()  {
	// Add theme support for Featured Images
	add_theme_support( 'post-thumbnails' );	

	// Add theme support for Semantic Markup
	$markup = array( );
	add_theme_support( 'html5', $markup );	
}

// Hook into the 'after_setup_theme' action
add_action( 'after_setup_theme', 'custom_theme_features' );

}




if ( ! function_exists('custom_editor_styles') ) {

// Custom styles in content area
function custom_editor_styles() {
add_editor_style('/src/css/backend.css');
}
add_action( 'init', 'custom_editor_styles' );

}




/* Add nav menu */
if ( ! function_exists( 'custom_navigation_menus' ) ) {

function custom_navigation_menus() {

	$locations = array(
		'Primary' => __( 'Primary menu', 'likabra' ),
	);
	register_nav_menus( $locations );

}
add_action( 'init', 'custom_navigation_menus' );

}






/* Add sidebar */
if ( ! function_exists( 'custom_sidebar' ) ) {

function custom_sidebar() {

	register_sidebar(array(
	  'name' => __( 'Höger' ),
	  'id' => 'right-sidebar',
	  'description' => __( 'Widgets in this area will be shown on the right-hand side.' ),
	  'before_title'  => '<h4 class="widgettitle">',
	  'after_title'   => '</h4>',	
	));
	
}
add_action( 'widgets_init', 'custom_sidebar' );

}






/* Add image sizes */
if ( ! function_exists( 'custom_thumbnails' ) ) {

	add_image_size( 'small', 150, 150, true );
	add_image_size( 'medium', 900, 900, false );
	add_image_size( 'large', 800, 800, true );


	add_image_size( 'HD', 1920, 1080, true );
	add_image_size( 'Wide', 1920, 500, true );
	add_image_size( 'Pagewidth', 1200, 500, true );
	add_image_size( 'Landscape', 600, 400, true );
	add_image_size( 'Portrait', 400, 600, true );
	add_image_size( 'Square', 600, 600, true );

}





if ( ! function_exists( 'custom_pagination' ) ) {


function bootstrap_pagination( $query=null ) {
 
  global $wp_query;
  $query = $query ? $query : $wp_query;
  $big = 999999999;
 
  $paginate = paginate_links( array(
    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
    'type' => 'array',
    'total' => $query->max_num_pages,
    'format' => '?paged=%#%',
    'current' => max( 1, get_query_var('paged') ),
    'prev_text' => __('&laquo;'),
    'next_text' => __('&raquo;'),
    )
  );
 
  if ($query->max_num_pages > 1) :
?>
<ul class="pagination">
  <?php
  foreach ( $paginate as $page ) {
    echo '<li>' . $page . '</li>';
  }
  ?>
</ul>
<?php
  endif;
}

}



if ( ! function_exists( 'remove_page_parent_class' ) ) {
 
add_filter('nav_menu_css_class', 'current_type_nav_class', 10, 2);

function current_type_nav_class($classes, $item) {
    // Get post_type for this post
    $post_type = get_query_var('post_type');
 
    // Removes current_page_parent class from blog menu item
    if ( get_post_type() == $post_type )
        $classes = array_filter($classes, "get_current_value" );
  
    return $classes;
}
function get_current_value( $element ) {
    return ( $element != "current_page_parent" );
}
} 



// Add ACF Theme Settings Pages

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Tema',
		'menu_title'	=> 'Tema',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
		
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Sidfot',
		'menu_title'	=> 'Sidfot',
		'parent_slug'	=> 'theme-general-settings',
	));
	
}



// Add Fancybox class to images inserted in post content

if( function_exists('add_fancybox_image_links') ) {
 
function give_linked_images_class($html, $id, $caption, $title, $align, $url, $size, $alt = '' ){
  $classes = 'fancybox'; // separated by spaces, e.g. 'img image-link'
  $rel = 'group';
 
  // check if there are already classes assigned to the anchor
  if ( preg_match('/<a.*? class=".*?">/', $html) ) {
    $html = preg_replace('/(<a.*? class=".*?)(".*?>)/', '$1 ' . $classes . '$2', $html);
  } else {
    $html = preg_replace('/(<a.*?)>/', '$1 rel="' . $rel . '" class="' . $classes . '" >', $html);
  }
  return $html;
}
add_filter('image_send_to_editor','give_linked_images_class',10,8);

}