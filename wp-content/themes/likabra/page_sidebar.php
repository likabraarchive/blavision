<?php get_header(); ?>

      

<section id="page">
      <div class="container">

      	<section class="row">

	  			<div class="col-sm-9">
					<!-- Start the loop -->
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	
	
	
	
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					
					
						<div class="page-content">
							<?php the_title( '<header class="page-header"><h1>', '</h1></header>' ); ?>
							<?php if ( has_post_thumbnail()) {  ?>
							      <?php the_post_thumbnail('Pagewidth', array('class' => 'page-wide')); ?>
							<?php } ?>
							<?php the_content(); ?>
						</div><!-- .page-content -->
						<hr />
						<?php the_tags( '<footer class="entry-meta"><span class="tag-links">Taggar: ', ' ', '</span></footer><hr />' ); ?>
						
					
					</article><!-- #post-## -->
	
	
					<!-- End the loop -->
					<?php	endwhile;
							// Previous/next post navigation.
						else :
							// If no content...
						echo '<article class="col-sm-12">Ingenting hittades...</article>';
						endif;
					?>
	  			</div>
	  			<?php get_sidebar() ?>
		      		
      		
		</section><!-- end .row -->
	</div><!-- end .container -->
</section><!-- end #page -->


      
<?php get_footer(); ?>      


