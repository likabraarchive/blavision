<?php



if ( ! function_exists('custom_scripts_and_styles') ) {

// Register Scripts
function custom_scripts() {

	wp_register_script( 'bootstrap', get_template_directory_uri() . '/src/frameworks/bootstrap/js/bootstrap.min.js', array( 'jquery' ), false, true ); // load in footer
	wp_enqueue_script( 'bootstrap' );

	wp_register_script( 'fancybox', get_template_directory_uri() . '/src/frameworks/fancybox/jquery.fancybox.pack.js', array( 'bootstrap' ), false, true ); // load in footer
	wp_enqueue_script( 'fancybox' );

	wp_register_script( 'fancybox-buttons', get_template_directory_uri() . '/src/frameworks/fancybox/helpers/jquery.fancybox-buttons.js', array( 'fancybox' ), false, true ); // load in footer
	wp_enqueue_script( 'fancybox-buttons' );

	wp_register_script( 'slider', get_template_directory_uri() . '/src/frameworks/slick/slick.min.js', array( 'bootstrap' ), false, true ); // load in footer
	wp_enqueue_script( 'slider' );

	wp_register_script( 'equalheight', get_template_directory_uri() . '/src/js/grids.min.js', array( 'bootstrap' ), false, true ); // load in footer
	wp_enqueue_script( 'equalheight' );

	wp_register_script( 'global', get_stylesheet_directory_uri() . '/src/js/global.js', array( 'bootstrap' ), false, true ); // load in footer
	wp_enqueue_script( 'global' );

}
// Hook into the 'wp_enqueue_scripts' action
add_action( 'wp_enqueue_scripts', 'custom_scripts' );





// Ugly Custom scripts for IE (wp_enqueue_scripts can't handle If IE)
add_action( 'wp_head',  'IEFIXhead');

    function IEFIXhead() {
	echo '<!--[if lt IE 9]>';
    echo '<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>';
    echo '<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>';
    echo '<![endif]-->';
} 





// Register Styles
function custom_styles() {

	wp_register_style( 'bootstrap', get_template_directory_uri() . '/src/frameworks/bootstrap/css/bootstrap.min.css', array(), false );
	wp_enqueue_style( 'bootstrap' );

	wp_register_style( 'gravityforms', get_template_directory_uri() . '/src/css/gravityforms.css', array('bootstrap'), false );
	wp_enqueue_style( 'gravityforms' );

	wp_register_style( 'animate', get_template_directory_uri() . '/src/css/animate.css', array('bootstrap'), false );
	wp_enqueue_style( 'animate' );

	wp_register_style( 'fancybox', get_template_directory_uri() . '/src/frameworks/fancybox/jquery.fancybox.css', array('bootstrap'), false );
	wp_enqueue_style( 'fancybox' );

	wp_register_style( 'fancybox-buttons', get_template_directory_uri() . '/src/frameworks/fancybox/helpers/jquery.fancybox-buttons.css', array('fancybox'), false );
	wp_enqueue_style( 'fancybox-buttons' );

	wp_register_style( 'slider', get_template_directory_uri() . '/src/frameworks/slick/slick.css', array('bootstrap'), false );
	wp_enqueue_style( 'slider' );
	
	// wp_register_style( 'owltheme', get_template_directory_uri() . '/src/frameworks/owl/owl.theme.css', array('owl'), false );
	// wp_enqueue_style( 'owltheme' );

	wp_register_style( 'likabra', get_stylesheet_directory_uri() . '/src/css/likabra.css', array('bootstrap'), false );
	wp_enqueue_style( 'likabra' );
}
// Hook into the 'wp_enqueue_scripts' action
add_action( 'wp_enqueue_scripts', 'custom_styles' );


// Register Admin Style
function custom_admin_styles() {

	wp_register_style( 'admincss', get_stylesheet_directory_uri() . '/src/css/backend.css', array(), false );
	wp_enqueue_style( 'admincss' );
}
// Hook into the 'admin_enqueue_scripts' action
add_action( 'admin_enqueue_scripts', 'custom_admin_styles' );



}





