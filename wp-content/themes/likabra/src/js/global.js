jQuery(document).ready(function($) {
	var bumpIt = function() {
			$('html').css('min-height', $(window).height() - $('#wpadminbar').height());
			$('body').css('margin-bottom', $('footer.sticky').height());
			$('body.admin-bar').css('margin-bottom', $('footer.sticky').height() + 0);
		},
		didResize = false;
	bumpIt();
	$(window).resize(function() {
		didResize = true;
	});
	setInterval(function() {
		if (didResize) {
			didResize = false;
			bumpIt();
		}
	}, 250);
	

	$(".fancybox").fancybox({
		prevEffect		: 'none',
		nextEffect		: 'none',
		closeBtn		: false,
		helpers		: {
			title	: { type : 'inside' },
			buttons	: {}
		}
	});





});