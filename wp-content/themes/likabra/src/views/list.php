<!-- List-item template -->

<?php
$title = get_the_title();
$copy = get_the_content( );
$link = get_permalink();
$date = get_the_time('d');
$month = get_the_time('M');
$year = get_the_time('Y');


$image = '<div class="missingimage"><div class="date"><h3>' . $date . '</h3>' . $month . ' ' . $year . '</div></div>';

if (has_post_thumbnail()){
	$image = get_the_post_thumbnail(null, 'Portrait', array('class' => 'listimage'));
}

$str = <<<EOF



<article class="list-item col-sm-12">			
	<div class="row">
    	<a class="hidden-xs col-sm-3" href="$link">
			$image
		</a>
    	<div class="col-sm-9">
      		<h4><a href="$link" >$title</a></h4>
	  		<p>$copy</p>
	  	</div>
	</div>
</article><!-- .list-item -->



EOF;
    echo $str;


?>
