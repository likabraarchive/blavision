<!-- Puff template-->

<?php


$imageArray = get_sub_field("bild");
$imgUrl = $imageArray["sizes"]["Fullwidth"];
$link = get_sub_field("lank") ;
$rubrik = get_sub_field("rubrik") ;
$text = get_sub_field("text") ;





$str = <<<EOF




<article class="col-sm-4 grid-item">
	<a class="hidden-xs" href="$link">
		<img src="$imgUrl" />
	</a>
	<div class="grid-copy">
		<h4><a href="$link">$rubrik</a></h4>
		<p>$text</p>
	</div>
</article><!-- .grid-item -->
        




EOF;
    echo $str;


?>
