<!-- Kontakt template-->

<?php


$imageArray = get_sub_field("bild");
$imgUrl = $imageArray["sizes"]["Portrait"];

$namn = get_sub_field("namn") ;
$titel = get_sub_field("titel") ;
$telefon = get_sub_field("telefon") ;
$epost = get_sub_field("epost") ;





$str = <<<EOF



	<div class="row">
    	<div class="hidden-xs col-sm-2">
			<img src="$imgUrl" />
		</div>
    	<div class="col-sm-10">
			<h4>$namn</h4>
			<p><strong>$titel</strong></p>
			<p><span class="glyphicon glyphicon-phone">&nbsp;</span> $telefon<br>
			<span class="glyphicon glyphicon-envelope">&nbsp;</span> <a href="mailto:$epost">$epost</a></p>
	  	</div>
	</div>
    <hr />
    




EOF;
    echo $str;


?>
