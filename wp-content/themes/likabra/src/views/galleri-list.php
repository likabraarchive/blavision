<!-- Grid-item template -->

<?php
$title = get_the_title();
$copy = get_the_excerpt( );
$link = get_permalink();
$image = '<img class="gridimage" src="' . get_stylesheet_directory_uri() . '/src/images/default.png"/>';

$numberofimages = "6";

if (has_post_thumbnail()){
	$image = get_the_post_thumbnail(null, 'medium', array('class' => 'galleryimg'));
}

$str = <<<EOF



<a href="$link" class="col-sm-3 galleri-item" style="margin-bottom:30px;">
		<h4>$title<br>($numberofimages bilder)</h4>
		$image
</a><!-- .galleri-item -->



EOF;
    echo $str;


?>
