<!-- Slideshow template-->

<?php

/* Variables */
$imageArray = get_sub_field("bild");
$imgUrl = $imageArray["sizes"]["Fullwidth"];
$link = get_sub_field("knapp") ;
$rubrik = get_sub_field("rubrik") ;
$text = get_sub_field("text") ;
$knapptext = get_sub_field("knapptext") ;
$button = "";


/* Conditional elements */
if ($link){
	$button = '<a href="' . $link . '" class="btn btn-lg btn-default">' . $knapptext . '</a>';
}




$str = <<<EOF

    <div class="superslide" style="background-image: url($imgUrl);">
      <div class="wrapper">
	      	<h1>$rubrik</h1>
	      	<div class="lead">$text</div>
	      	$button
      </div>
    </div>

EOF;

echo $str;


?>
