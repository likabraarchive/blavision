<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/src/images/ico/favicon.ico">

    <title><?php wp_title(); ?></title>
    
    <?php wp_head(); ?>

    
    <style>
    	.container {
	    	max-width: <?php the_field('sidbredd', 'option'); ?>px;
    	}
    </style>
   <?php
	
	//Set default values
	$color = "transparent";
	$image ="none";
	$repeat = "repeat";
	$position = "initial";
	$size = "auto";
	$attachment = "inherit";
	$contentbg = "transparent";
	$contentcolor = "inherit";
	
	//Get values
	$newfarg = get_field('farg', 'option');
	$newbild = get_field('bild', 'option');
	$newfarg_content = get_field ('farg_content', 'option');
	$newcontentcolor = get_field('textfarg', 'option');
	$newimagesetting = get_field('bildinstallningar', 'option');
	
	//Get img-url from array
	if( !empty($newbild) ):
		$newbild = $newbild['sizes'][ 'HD' ];
	endif;

	//Set ACF-values		
	if($newfarg ):
		$color = $newfarg;
	endif;
	
	if($newfarg_content ):
		$contentbg = $newfarg_content;
	endif;

	if($newcontentcolor ):
		$contentcolor = $newcontentcolor;
	endif;
	
	if($newbild ):
		$image = "url(" . $newbild . ")";
	endif;

	if($newimagesetting == "no-repeat" ):
		$repeat = "no-repeat";
		$size = "cover";
		$position = "center top";
	endif;
	
	if($newimagesetting == "fixed" ):
		$repeat = "no-repeat";
		$size = "cover";
		$position = "center top";
		$attachment = "fixed";
	endif;
?>


<style>
	body {
		background-color: <?php echo $color; ?>;
		background-image: <?php echo $image; ?>;
		background-repeat: <?php echo $repeat; ?>;
		background-position: <?php echo $position; ?>;
		background-size: <?php echo $size; ?>;
		background-attachment: <?php echo $attachment; ?>
	}
	.container {
		background-color:  <?php echo $contentbg; ?>;
		color: <?php echo $contentcolor; ?>
	}
</style>
 
    
    
  </head>

  <body <?php body_class(); ?>>












	

      <!-- Fixed navbar -->
	  <nav class="navbar navbar-default navbar-fixed-top navBig" role="navigation">
	      <!-- Brand and toggle get grouped for better mobile display -->
	      <div class="container">
	          <div class="navbar-header">
	              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
	                  <span class="sr-only">Toggle navigation</span>
	                  <span class="icon-bar"></span>
	                  <span class="icon-bar"></span>
	                  <span class="icon-bar"></span>
	              </button>
	  
	              <a class="navbar-brand" href="<?php echo home_url(); ?>">
	              		<?php if( get_field('logo', 'option') ): ?>
				  			<img src="<?php the_field('logo', 'option'); ?>" />
				  		<?php else: ?>
				  			<?php wp_title(); ?>
				  		<?php endif; ?>
	              </a>
	          </div>
	  
	          <?php
	              wp_nav_menu( array(
	                  'theme_location'    => 'Primary',
	                  'depth'             => 2,
	                  'container'         => 'div',
	                  'container_class'   => 'collapse navbar-collapse navbar-ex1-collapse',
	                  'menu_class'        => 'nav navbar-nav navbar-right',
	                  'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
	                  'walker'            => new wp_bootstrap_navwalker())
	              );
	          ?>
	      </div>
	  </nav>      

	  <div class="nav-margin navBig"></div>